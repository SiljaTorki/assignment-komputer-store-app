//Declaring and storing DOM-elements
let laptopsElement = document.getElementById("laptops");
let laptopsFeatureElement = document.getElementById("laptopFeature");
let laptopNameElement = document.getElementById("laptopName");
let laptopPictureElement = document.getElementById("laptopPicture");
let laptopDescriptionElement = document.getElementById("laptopDescription");
let laptopPriceElement = document.getElementById("laptopPrice");
let buyLeptopElement = document.getElementById("buyLaptop");

let buyButton = document.getElementById("button_buy");


//State of what is needed in the application
//Representation of data, will be loaded into DOM-element eventually
let laptops = [];
let images = [];
let totalPay = 0.0;
let titleLap = "";
let laptopSpecs = [];
let laptopImage = [];

//Fetch the laptopss from API
fetch("https://noroff-komputer-store-api.herokuapp.com/computers") // returns a promise
    .then(response => response.json()) //chain the fetch with then, returns some of the data from the API fetched in line above
    .then(data => laptops = data) // Assign the leptops value to the data
    .then(laptops => addLaptopsToMenu(laptops));  //call laptops to menu

//Fetch the laptops pictures from API


// Add laptop specs to DOM
const addLaptopsToMenu = (laptops) => {

    // For each laptop in laptopss, and all laptop to optoin menu
    laptops.forEach(laptop => addLaptopToMenu(laptop));
    
    laptopSpecs = laptops[0].specs;
    let out = laptopSpecs.join(" \n ");

    laptopsFeatureElement.innerText = out;   
}


// Add inividual laptop to the function addLaptopsToMenu
const addLaptopToMenu = (laptop) => {
    // Create HTML-element that will print on site
    const laptopElement = document.createElement("option");

    // Add infomation to the optionHTML-elment
    laptopElement.value = laptop.id;  //Id is uniqe
    laptopElement.appendChild(document.createTextNode(laptop.title)); // Append child elment to HTML element, adding discription to Drink
    
    // Add option to the DOM
    laptopsElement.appendChild(laptopElement); 
}

//Function
const handleLaptopMenuChange = e => {

    buyLeptopElement.style.visibility = "visible";
    //Returns choocen laptop info
    const selectedLaptop = laptops[e.target.selectedIndex];
    laptopSpecs = selectedLaptop.specs;
    laptopsFeatureElement.innerText = laptopSpecs.join(" \n ");
    laptopNameElement.innerText = selectedLaptop.title;
    titleLap = selectedLaptop.title;
    laptopDescriptionElement.innerText = selectedLaptop.description;
    laptopPriceElement.innerText = new Intl.NumberFormat('no-NO',  { style: 'currency', currency: 'NOK' }).format(selectedLaptop.price);
    totalPay = selectedLaptop.price;
    laptopPictureElement.src = `https://noroff-komputer-store-api.herokuapp.com/${selectedLaptop.image}`
}

// Handle buy button
const handleBuyLaptop = () => {

    console.log(userProfile.balance + userProfile.remindingLoan);
    // Validates whether the bank balance is sufficient to purchase the selected laptop
    if(totalPay > userProfile.balance + userProfile.remindingLoan) {
        let dif = new Intl.NumberFormat('no-NO',  { style: 'currency', currency: 'NOK' }).format(totalPay - userProfile.balance);
        alert(`You cannot afford the laptop. You need: ${dif} before you can afford the laptop!`);
    } else {

        if(userProfile.remindingLoan != 0) {
            totalPay -= userProfile.remindingLoan;
            userProfile.remindingLoan = 0;
        }

        console.log(totalPay);
        userProfile.balance -= totalPay;
        console.log(userProfile.balance);
        bankBalanceElement.innerText = new Intl.NumberFormat('no-NO',  { style: 'currency', currency: 'NOK' }).format(userProfile.balance);
        alert(`Congratulations you are now the owner of the new laptop ${titleLap}!`);
    }

}


//Eventlistner
laptopsElement.addEventListener("change", handleLaptopMenuChange);
buyButton.addEventListener("click", handleBuyLaptop);
