//Declaring and storing DOM-elements
let payElement = document.getElementById("payWork");

//Buttons
let bankButton = document.getElementById("button_bank");
let workButton = document.getElementById("button_work");
let replayLoanButton = document.getElementById("button_replayLoan");

//Representation of data, will be loaded into DOM-element eventually
let outstandingLoan = 0;


// Change payment DOM, NOT part of bank balance!
payElement.innerText = new Intl.NumberFormat('no-NO',  { style: 'currency', currency: 'NOK' }).format(userProfile.pay);

// Event happning when click on Bank button
//Transfer the money from your Pay/Salary balance to your Bank balance
//Reset your pay/salary once you transfer
const handleTransfer = () => {
    outstandingLoanElement.innerText = "Outstanding Loan";

    //If you have an outstanding loan, 
    //10% of your salary WILL first be deducted 
    //and transferred to the outstanding Loan amount
    if(userProfile.remindingLoan != 0){
        outstandingLoan = (userProfile.pay/100)*10;
        userProfile.remindingLoan -= outstandingLoan;
        userProfile.balance += (userProfile.pay/100)*90;
        userProfile.pay = 0;
    } else {
        userProfile.balance += userProfile.pay;
        userProfile.pay = 0;
    }

    //Add to DOM
    loneValueElement.innerText = new Intl.NumberFormat('no-NO',  { style: 'currency', currency: 'NOK' }).format(userProfile.remindingLoan);
    bankBalanceElement.innerText = new Intl.NumberFormat('no-NO',  { style: 'currency', currency: 'NOK' }).format(userProfile.balance);
    payElement.innerText = new Intl.NumberFormat('no-NO',  { style: 'currency', currency: 'NOK' }).format(userProfile.pay);
}

// Increase Pay balance by clicking work button
const handleIncrease = () => {
    userProfile.pay += 100;
    payElement.innerText = new Intl.NumberFormat('no-NO',  { style: 'currency', currency: 'NOK' }).format(userProfile.pay);
} 

//The full value of your current Pay amount 
//should go towards the outstanding loan and NOT your bank account
const handleCurrentPayAmount = () => {
    let rest = 0;

    //if the loan is bigger than what the user wants to pay back 
    // else the loan will be pays back and the rest goes to the User balance
    if(userProfile.pay < userProfile.remindingLoan) {
        payBackLoan(userProfile.pay);
    } else {
        rest += userProfile.pay - userProfile.remindingLoan;
        userProfile.balance += rest;
        payBackLoan(userProfile.pay-rest);
    }

    
    //payBackLoan(userProfile.pay);
    userProfile.pay = 0;
    
    bankBalanceElement.innerText = new Intl.NumberFormat('no-NO',  { style: 'currency', currency: 'NOK' }).format(userProfile.balance);
    payElement.innerText = new Intl.NumberFormat('no-NO',  { style: 'currency', currency: 'NOK' }).format(userProfile.pay);
}


//Addig an eventlistner to get buttons
bankButton.addEventListener("click", handleTransfer);
workButton.addEventListener("click", handleIncrease);
replayLoanButton.addEventListener("click", handleCurrentPayAmount);