# Assignment Komputer Store App

## Table of Contants
- [Install](#install)
- [Usage](#usage)
- [Description](#description)
- [Maintainers](#maintainers)
- [Visuals](#visuals)
- [Project Status](#project_status)

## Install

Clone git repository:


![CLONE](./images/CloneButton.PNG)

## Usage
1. Clone Repository
2. Open in a text editor (e.g. Visual Studio Code)
3. Then open html file with Live Server


## Description
A dynamic webpage using “vanilla” JavaScript. 

- The bank shows a “Bank” balance in your currency. This is the amount available for you to buy a laptop.
- The Get a loan button will attempt to get a loan from the bank.
- The pay or your current salary amount in your currency. Shows how much money you have earned by “working”. This money is NOT part of your bank balance.
- The bank button must transfers the money from your Pay/Salary balance to your Bank balance.
- The work button increases your Pay balance at a rate of 100 on each click.
- Once you have a loan, a new button labeled “Repay Loan” appears. Upon clicking this button, the full value of your current Pay amount should go towards the outstanding loan and NOT your bank account.
- Laptops -  ShowS the available computers. The feature list of the selected laptop is displayed here
- The buy now button is the final action of your website. This button will attempt to “Buy” a laptop and validate whether the bank balance is sufficient to purchase the selected laptop. If you do not have enough money in the “Bank”, a message must be shown that you cannot afford the laptop. 


## Maintainers
Silja Stubhag Torkildsen - @SiljaTorki


## Visuals

### Wireframe:

https://www.figma.com/file/vSXOVHjxYETCzaBVSIFarW/The-Komputer-Store?node-id=3%3A2

![Wireframe](./images/figma.PNG)

### 1) The Bank – an area where you will store funds and make bank loans

Functions :
- showLoan(array) -> Shows the outstanding Loan value
- payBackLoan(moneyToPayBack) -> Calculates pay back lone
- handleGetLoan() -> Get loan when click "Get Loan" button

Events:
- "Get Loan" button -> click

The Bank:

![TheBank](./images/theBank.PNG)


### 2) Work – an area to increase your earnings and deposit cash into your bank balance

Functions:
- handleTransfer() -> Transfer the money from your Pay balance to your Bank balance, and resets your pay/salary once you transfer when click "Bank" button
- handleIncrease() -> Increase Pay balance by click "Work" button
- handleCurrentPayAmount() -> //The full value of your current Pay amount goes to\ the outstanding loan and NOT your bank account when click "Replay Loan"

Events:
- "Bank" button -> click
- "Work" button -> click
- "Replay Loan" button -> click

Work:

![Work](./images/work.PNG)


### 3) Laptops – an area to select and display information about the merchandise

Functions:
- Fetch API from "https://noroff-komputer-store-api.herokuapp.com/computers" and calls to menu
- addLaptopsToMenu(laptops) -> Adds laptop specs
- addLaptopToMenu(laptop) -> Add inividual laptop to the function addLaptopsToMenu
- handleLaptopMenuChange(e) -> Handels changes when laptop option changed with <select>-element
- handleBuyLaptop() -> Attempt to “Buy” a laptop and validate whether the bank balance is sufficient to purchase the selected laptop

Events:
- Select laptop option -> change
- "Buy Now" button - click

Laptop:

[!Laptop](/images/laptop.PNG)


Selected Laptop:

[!SelectedLaptop](/images/buyLaptop.PNG)


## Project status

Project complete!


