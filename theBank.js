//Declaring and storing DOM-elements
let bankBalanceElement = document.getElementById("userCxurrency");
let nameElement = document.getElementById("user");

let getLoanButton = document.getElementById("button_loan");

//Only visible after taking loan
let outstandingLoanElement = document.getElementById("outLoan");
let loneValueElement = document.getElementById("loanValue");
//let loanToPayBack = 0;

//User
const userProfile = {
    name: 'Joe Baker',
    balance: 200,
    pay: 1000,
    remindingLoan: 0
}

//Representation of data, will be loaded into DOM-element eventually
let change = 0;
let paybackchange = 0;


//Adding name of user to Dom
nameElement.innerText = userProfile.name;

//Adding the amount available to buy a laptop, in NOK
let balanceNum = userProfile.balance;
bankBalanceElement.innerText = new Intl.NumberFormat('no-NO',  { style: 'currency', currency: 'NOK' }).format(balanceNum);

//Array to add loans
let loan =[];


//Shows the outstanding Loan value
const showLoan = (array) => {
    outstandingLoanElement.innerText = "Outstanding Loan";

    //Make “Repay Loan” visible
    replayLoanButton.style.visibility = "visible";

    for (let index = 0; index < array.length; index++) {
        let loanValue= array[index];
        
        userProfile.remindingLoan += loanValue;
        loneValueElement.innerText = new Intl.NumberFormat('no-NO',  { style: 'currency', currency: 'NOK' }).format(userProfile.remindingLoan);
    }
}
// Pay back lone
function payBackLoan (payBack){
    for (let index = 0; index < loan.length; index++) {
        userProfile.remindingLoan -= payBack;
        console.log(userProfile.remindingLoan)

        if(userProfile.remindingLoan != 0) {
            alert(`You have payd back some of your loan! Reminding: ${userProfile.remindingLoan}`);
            loan[index] = userProfile.remindingLoan;
            loneValueElement.innerText = new Intl.NumberFormat('no-NO',  { style: 'currency', currency: 'NOK' }).format(userProfile.remindingLoan);
        } else {
            //let rest = 
            //Hide “Repay Loan” visible
            replayLoanButton.style.visibility = "hidden";
            loan.pop();

            userProfile.remindingLoan = 0;
            console.log(userProfile.remindingLoan)
            loneValueElement.innerText = new Intl.NumberFormat('no-NO',  { style: 'currency', currency: 'NOK' }).format(userProfile.remindingLoan);
        }
    }
    
}

//Get a loan
const handleGetLoan = () => {

    //Prompt that register user loan input
    let amountToLoan = prompt("Please enter the amount of money you wish to loan:");
    change = parseFloat(amountToLoan);
    
    console.log(loan);
    
    if(change > (userProfile.balance*2)) {
        alert('You cannot get a loan more than double of your bank balance!');
    } else if(userProfile.remindingLoan != 0) {
        alert('You cannot get more than one bank loan before repaying the last loan!')
    } else {
        alert(`Loan successful! You have loan: ${change.toFixed(2)}`);
        loan.push(change);
        console.log(loan);
        showLoan(loan);
    }
}



//Addig an eventlistner to get loan button
getLoanButton.addEventListener("click", handleGetLoan);
